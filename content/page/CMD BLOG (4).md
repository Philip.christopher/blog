#Blog

####Dit is een blog waar ik in CMD jaar 1 allemaal heb gedaan en wat ik ervan heb geleerd.
---

**Woensdag 30 Aug**

Vandaag was de eerste lesdag van CMD. We hebben eerst een introductie over wat we op CMD de komende 4 jaar allemaal gaan doen, hoeveel studiepunten we moeten halen om over te gaan naar de volgend jaar of voor je propedeuse enz. 

Na de introductie, worden we (Klas 1A en 1B) naar de studio gebracht, dat is nieuw voor mij, om samen met meerdere klassen in een lokaal te zitten, want 2 jaar geleden was de module in CMD anders.

We gingen in de studio een team vormen en we moesten dan een mascotte gaan maken. Daar heb ik geleerd om samen te werken met de andere mensen van de team.

---
**Donderdag 31 Aug**

De dag begon om 12:00 met Hoorcollege over Game Design. Ik vond het een interessante les, want het was goed gepresenteerd en de context over game vind ik interessant. Ik heb daar geleerd over wat een game is en wat een game moet hebben, namelijk: Doel, Regels, en Keuzes.

Daarna ging ik samen met mijn groep de 'Rotterdam Tour' doen. We moesten door Rotterdam lopen en de toeristische plekken bezoeken.
Ik heb gemerkt dat er heel veel kunstwerken zijn in Rotterdam, bijvoorbeeld 'De Drol' van Willem de Kooning. Door de tour leer ik ook meer over mijn teamgenoten.

---
**Maandag 4 Sep**

We kregen een briefing over Design Challenge, een groepsopdracht waar ik deze kwartaal bezig zal zijn. De opdracht is om een game te maken voor de introductieweek van de nieuwe HR studenten. Om dat te doen moeten we eerst gaan onderzoeken. We hebben die dag ook gelijk de taken verdeeld, zodat we thuis mee verder kan werken en zodat het proces sneller loopt. 

---
**Dinsdag 5 Sep**

We kregen vandaag een hoorcollege over Design Theory van Raul. Ik vond het zelf een leuke en interessante hoorcollege, het was duidelijk en met plezier uitgelegd. Ik heb ook veel van gekregen van de presentatie, zoals wat 'design' eigenlijk is, en de filosofie's achter design. Dat heb ik gemist toen ik 2 jaar geleden in CMD zat.

---
**Woensdag 6 Sep**

Woensdag was weer een studiodag. Ik ging verder met mijn onderzoek over de stad Rotterdam. We hebben samen ook een doelgroep gekozen, namelijk de Graphics Design van WDKA. De reden hiervoor is dat een van onze lid van de team daar heb gezeten, dus makkelijker om het verder te gaan onderzoeken.

Daarna heb ik een workshop gevolgd over Blog van Nio. Daar heb ik geleerd hoe ik een blog moet gaan maken dmv. Markdown. Ik vind het veel simpeler en makkelijker dan html, en dat vind ik heel fijn. 

Na de workshop, was het de Studieloopbaan coaching. We hebben de sterkte en de zwakte van onze team onderzocht doormiddel van een SWOT-analyse. Ik heb daar ook geleerd dat de module anders is, dat het nu omgaat met 'het leren van leren'. Ik vond het een goede methode, omdat het handig is voor je hele leven lang is.

---
**Donderdag 7 Sep**

Op donderdag hadden we een werkcollege van Design Theory. We moesten als huiswerk de samenvatting van de hoorcollege van de vorige dinsdag meenemen. Met behulp van de aantekeningen moet ik samen met mijn groep een presentatie houden over een voorbeeld van de proces van design, namelijk:

> **Huidige Situatie** -> **Process** -> **strong text**

We hebben gepresenteerd en we hebben als voorbeeld een bedrijf die een website wilt maken voor hun product. Verder hebben we de moeilijke termen herhaald zoals bijvoorbeeld Problem- en Solution Space, Wicked Problems enz.

---
**Vrijdag 8 Sep**

Het was eigenlijk een vrije dag voor ons omdat er geen workshop was, maar onze groep heeft zichzelf over getroffen. We gingen dus verder met onze onderzoek, we hebben onze doelgroep al gekozen, dus we dachten om ze persoonlijk te gaan interviewen om meer te weten over hen en wat voor spel geschikt is voor ze. We hebben die dag 4  tweedejaars studenten van 'Graphics Design' geïnterviewd.

Van de interviews hebben we veel interessante dingen geleerd over onze doelgroep. Ze hebben Rotterdam beschreven als: 'Multiculturele, Artsy, Leuke stad.' Ze vinden allemaal ook "Architectuur" een leuke thema. Dus hebben ook besloten dat onze thema van het spel over Architectuur zal worden.




---

